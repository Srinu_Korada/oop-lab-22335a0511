import java.util.Scanner;
class Synchronized extends Thread{
    public void run()
    {
        System.out.println("Enter two numbers : ");
        Scanner input = new Scanner(System.in);
        int num1 = input.nextInt();
        int num2 = input.nextInt();
        multiply(num1);
        multiply(num2);
    }
    synchronized void multiply(int n)
    {
        for(int i = 1 ;i<=5 ;i++)
        {
            System.out.println(n + " * " + i + " = " + n*i);
            try {
                Thread.sleep(500);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }
    public static void main(String[] args) {
        SynchronizedThread obj = new SynchronizedThread();
        obj.start();
    }
}

