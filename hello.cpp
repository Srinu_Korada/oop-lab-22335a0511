#include <iostream>
#include <string.h>
using namespace std;
int main()
{
    char name[100];
    cout << "Enter Your Username : " << endl;
    cin >> name;
    bool c = false;
    for (int i = 0; i < strlen(name); i++)
    {
        if (isdigit(name[i]) || !(isalpha(name[i])))
        {
            c = true;
            break;
        }
    }
    if (c)
    {
        cout << "ERROR" << endl;
    }
    else
    {
        cout << "Hello " << name << endl;
    }
    return 0;
}

