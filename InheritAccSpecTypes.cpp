#include <iostream>
using namespace std;
class Shape
{
public:
   void setWidth(int w)
   {
       width = w;
   }
   void setHeight(int h)
   {
       height = h;
   }
   void setLength(int l)
   {
       length = l;
   }
protected:
   int width;
   int height;
   int length;
};
class Rectangle: public Shape
{
public:
   int getVolume()
   {
       return (width * height * length);
   }
};
int main(void)
{
   Rectangle Rect;
   Rect.setWidth(4);
   Rect.setHeight(2);
   Rect.setLength(6);
   cout << "Total Volume of a rectangle: " << Rect.getVolume() << endl;
   return 0;
}
