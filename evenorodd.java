import java.util.Scanner;

class EvenOrOdd {
    public static void evenOdd(int n) {
        System.out.print("The Given number is ");
        if (n % 2 == 0)
            System.out.println("Even");
        else
            System.out.println("Odd");
    }
    public static void main(String[] args) {
        System.out.print("Enter a number : ");
        Scanner number = new Scanner(System.in);
        evenOdd(number.nextInt());
    }
}
